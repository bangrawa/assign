package com.recko.assign.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.tomcat.util.buf.UDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.recko.assign.domain.sql.UFPersonEntity;
import com.recko.assign.domain.sql.UniverseFamilyRelnEntity;
import com.recko.assign.repo.sql.UFPersonRepository;
import com.recko.assign.repo.sql.UniverseFamilyRelnRepository;

@Component
public class FamilyService {
	
	@Autowired
	private UniverseFamilyRelnRepository repo;
	
	@Autowired
	private UFPersonRepository ufRepository;

	public Map<String, Object> powerOfFamilyAcrossUniverses(Integer fId){
		Map<String, Object> result = new HashMap<>();
		result.put("status", "success");
		
		List<UniverseFamilyRelnEntity> entities = repo.findAllByFId(fId);
		
		List<String> ufIds = entities.stream().map(e -> e.getUfId()).collect(Collectors.toList()); 
		
		if(ufIds.isEmpty()) {
			result.put("msg", "this family does not exist in any universe");
			return result;
		}
		int power = calculatePower(ufIds.get(0));
		for(int i = 1; i < ufIds.size(); i++) {
			if(power != calculatePower(ufIds.get(i))) {
				result.put("mas", "power is not same acroos universes");
				return result;
			}
		}
		result.put("msg", "power is same acroos universes");
		return result;
	}
	
	// for a combination of family and universe calculates total power
	private Integer calculatePower(String ufId) {
		Integer power = 0;
		
		List<UFPersonEntity> entities = ufRepository.findAllByUfId(ufId);
		
		List<Integer> powers = entities.stream().map(e -> e.getPower()).collect(Collectors.toList());
		
		for(int i = 0; i < powers.size(); i++) {
			power += powers.get(i);
		}
		return power;
	}
	
	// 1. a family is balanced if total power of it's members is zero
	// 2. if a family with is with even # of members, is unbalanced we will change the power of some of the persons in order to banlance it
	// 3. if a family with is with even # of members, We will have add/remove 1 person to this family and then it conerts to case 2.
	// a person in a particular universe can only belong to one family
	public void balanceFamilies() {
		
		List<UniverseFamilyRelnEntity> entities = repo.findAll();
		// get all the family ids
		List<Integer> families = entities.stream().map(q -> q.getFId()).collect(Collectors.toList());
		
		//for each family
		for(int i = 0; i < families.size(); i++) {
			balance(families.get(i), entities);
		}
		
	}
	
	private void balance(Integer familyId, List<UniverseFamilyRelnEntity> entities) {
		//get all ufIds for this family
		List<String> ufIds = entities.stream().filter(e -> e.getFId() == familyId).map(e -> e.getUfId()).collect(Collectors.toList());
		// all the persons in this family in all the unvirses
		List<UFPersonEntity> persons = ufRepository.findAllByUfId(ufIds);
		
		int power = 0;
		for(int i = 0; i < persons.size(); i++) {
			power += persons.get(i).getPower();
		}
		
		// balanced
		if(power == 0) {
			return;
		}
		
		int count = power;
		if(count < 0) {
			count *= -1;
		}
		// we need to fix count/2 person's power in order to balance this family
		if(count % 2 != 0) {
			int index = 0;
			while(persons.get(index).getPower() != power && index < persons.size()) {
				index++;
			}
			persons.remove(index);
		}
		
		int index = 0;
		for(int i = 0; i < count/2; i++) {
			
			while(persons.get(index).getPower() != power && index < persons.size()) {
				index++;
			}
			// alter the power for count/2 persons
			persons.get(index).setPower(power * -1);
		}
		ufRepository.saveAll(persons);
		
	}
	
	
	
}
