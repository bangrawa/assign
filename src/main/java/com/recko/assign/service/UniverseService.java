package com.recko.assign.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.recko.assign.repo.sql.UniverseFamilyRelnRepository;
import com.recko.assign.domain.sql.UniverseFamilyRelnEntity;

@Component
public class UniverseService {
	
	@Autowired
	private UniverseFamilyRelnRepository repository;

	public Map<String, Object> getFamiliesByUId(Integer uId){
		
		Map<String, Object> result = new HashMap<>();
		result.put("status", "success");
		
		List<UniverseFamilyRelnEntity> entities = repository.findAllByUId(uId);
		
		List<Integer> families = entities.stream().map(e -> e.getFId()).collect(Collectors.toList());
		
		if(families.isEmpty()) {
			result.put("msg", "no families for this universe");
			return result;
		} 
		
		result.put("families", families);
		
		return result;
	}
	
}
