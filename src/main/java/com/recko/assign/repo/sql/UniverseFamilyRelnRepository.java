package com.recko.assign.repo.sql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.recko.assign.domain.sql.UniverseFamilyRelnEntity;

@Repository
public interface UniverseFamilyRelnRepository extends JpaRepository<UniverseFamilyRelnEntity, String> {
	
	List<UniverseFamilyRelnEntity> findAllByFId(Integer fId);
	
	List<UniverseFamilyRelnEntity> findAllByUId(Integer uId);

}
