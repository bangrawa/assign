package com.recko.assign.repo.sql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.recko.assign.domain.sql.UFPersonEntity;

@Repository
public interface UFPersonRepository extends JpaRepository<UFPersonEntity, String>{
	
	List<UFPersonEntity> findAllByUfId(String ufId);
	
	List<UFPersonEntity> findAllByUfId(List<String> ufIds);

}
