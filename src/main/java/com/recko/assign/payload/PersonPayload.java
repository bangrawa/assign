package com.recko.assign.payload;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown=true)
public class PersonPayload {
	
	private Integer personId;
	private List<PersonDetails> details;
	
	@Getter
	@Setter
	@JsonIgnoreProperties(ignoreUnknown=true)
	public class PersonDetails{
		private Integer universeId;
		private Integer familyId; // family id for this person in this universe: 'universeId'
		private Integer power; // power of person for universe = universeId
	}

}
