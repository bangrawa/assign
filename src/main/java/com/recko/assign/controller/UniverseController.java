package com.recko.assign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.recko.assign.service.UniverseService;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path(value="/universe")
public class UniverseController {
	
	@Autowired
	private UniverseService universeService;
	
	
	// add universe API here which is not necessary as of now
	
	
	@GET
	@Path(value="/get/families")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFamiliesByUniverseId(@QueryParam("u_id") Integer uId) {
		
		Map<String, Object> result = universeService.getFamiliesByUId(uId);
				
		return Response.ok(result).build();
	}

}
