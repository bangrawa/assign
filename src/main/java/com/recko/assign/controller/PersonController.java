package com.recko.assign.controller;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.recko.assign.payload.PersonPayload;
import com.recko.assign.service.PersonService;

@Component
@Path(value="/person")
public class PersonController {
	
	@Autowired
	private PersonService personService;

	@POST
	@Path(value="/add/person")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPerson(PersonPayload payload) {
		
		Map<String, Object> result = personService.addPerson(payload);
		
		return Response.ok(result).build();
	}
	
}
