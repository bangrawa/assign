package com.recko.assign.controller;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.recko.assign.service.FamilyService;

@Component
@Path(value="/family")
public class FamilyController {
	
	@Autowired
	private FamilyService familyService;
	
	@GET
	@Path(value="/check")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkForSamePower(@QueryParam("f_id") Integer fId) {
		
		Map<String, Object> result = familyService.powerOfFamilyAcrossUniverses(fId);
				
		return Response.ok(result).build();		
	}
	
	@POST
	@Path(value="/balance")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response balanceFamilies() {
		
		Map<String, Object> result = new HashMap<>();
		familyService.balanceFamilies();
		result.put("status", "success");
		
		return Response.ok(result).build();
		
	}

}
