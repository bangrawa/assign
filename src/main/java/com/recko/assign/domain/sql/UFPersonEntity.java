package com.recko.assign.domain.sql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="uf_pp_relation")
public class UFPersonEntity {
	
	@Id
	@Column(name="uf_id", columnDefinition="varchar(36)")
	private String ufId;
	
	@Column(name="p_id")
	private Integer pId;
	
	@Column(name="power")
	private Integer power;

}
