package com.recko.assign.domain.sql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data 
@Entity
@Table(name="uni_fam_relation")
public class UniverseFamilyRelnEntity {
	
	@Id
	@Column(name="uf_id", columnDefinition="varchar(36)")
	private String ufId;
	
	@Column(name="u_id")
	private Integer uId;
	
	@Column(name="f_id")
	private Integer fId;

}
